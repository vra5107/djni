DJni
====

A convenient D wrapper around JNI.

How to build DJni

Clone DJni
	
		git clone https://bitbucket.org/vra5107/djni.git 
	
Compile DJni
	
		dmd -oflibDJni.a -lib -H -Hdimport/jni jni/*.d

The above command does two things.

- Generates a shared library called libDJni.a

- Generates D import iles in import/jni directory.

// D import file generated from 'source/jni/JavaVM.d'
module jni.JavaVM;
import std.stdio;
import std.algorithm;
import std.string;
import jni.jni;
import jni.JavaEnv;
struct JniJavaVM
{
	public 
	{
		static const jint JNI_DEFAULT_VERSION = JNI_VERSION_1_6;
		this(JavaVM* v, jint ver = JNI_DEFAULT_VERSION)
		{
			val = v;
			_version = ver;
			doDestroy = false;
		}
		this(jint ver)
		{
			val = null;
			_version = ver;
			doDestroy = true;
			JNIEnv* env;
			JavaVMInitArgs args;
			args._version = _version;
			args.nOptions = 0;
			if (JNI_CreateJavaVM(&val, cast(void**)&env, &args) != 0)
			{
				writeln("Error: cannot create JavaVM!");
			}
		}
		this(string classPath, jint ver = JNI_DEFAULT_VERSION)
		{
			val = null;
			_version = ver;
			doDestroy = true;
			JNIEnv* env;
			JavaVMInitArgs args;
			args._version = _version;
			JavaVMOption[1] options;
			args.nOptions = 1;
			string argCP = "-Djava.class.path=" ~ classPath;
			options[0].optionString = cast(char*)std.string.toStringz(argCP);
			args.options = options.ptr;
			if (JNI_CreateJavaVM(&val, cast(void**)&env, &args) != 0)
			{
				writeln("Error: cannot create JavaVM!");
			}
		}
		this(this);
		this(JniJavaVM v)
		{
			val = v.val;
			_version = v._version;
			doDestroy = v.doDestroy;
			v.doDestroy = false;
		}
		this(ref const(JniJavaVM) v)
		{
			val = cast(JavaVM*)v.val;
			_version = v._version;
			doDestroy = false;
		}
		~this();
		ref JniJavaVM opAssign(JniJavaVM v);
		ref JniJavaVM opAssign(ref const(JniJavaVM) v);
		JavaVM* Val();
		const const(JavaVM)* Val();
		const jint Version();
		const bool Valid();
		const bool opEquals(JniJavaVM vm);
		const bool opEquals(ref const(JniJavaVM) vm);
		const JavaEnv GetEnv();
		private 
		{
			JavaVM* val;
			jint _version;
			bool doDestroy;
			void Destroy();
		}
	}
}
JniJavaVM jniJavaVM();

// D import file generated from 'source/jni/JavaString.d'
module jni.JavaString;
import std.stdio;
import std.algorithm;
import jni.jni;
import jni.JavaEnv;
import jni.JavaObject;
struct JavaString
{
	public 
	{
		this(JavaEnv env, jstring c)
		{
			writefln("%s(JavaEnv, jstring)", __FUNCTION__);
			base.__ctor(env, c);
			Init();
		}
		this(JavaEnv env, string _val)
		{
			writefln("%s(JavaEnv, string)", __FUNCTION__);
			this(env, env.Val().NewStringUTF(std.string.toStringz(_val)));
		}
		this(JavaString str)
		{
			writefln("%s(JavaString)", __FUNCTION__);
			swap(base, str.base);
			swap(val, str.val);
			str.base = JavaObject();
			str.val = null;
		}
		this(ref JavaString str)
		{
			writefln("%s(ref JavaString)", __FUNCTION__);
			base.__ctor(str.base);
			Init();
		}
		this(this);
		~this();
		ref JavaString opAssign(JavaString v);
		ref JavaString opAssign(ref JavaString v);
		const bool opEquals(ref const(JavaString) str);
		const bool opEquals(string str);
		const jstring Val();
		const string Value();
		const jsize Length();
		const jsize Size();
		const char opIndex(uint n);
		JavaObject base;
		alias base this;
private 
		{
			string val;
			void Init();
			void Destroy();
			protected void Reset(ref const(JavaString) str);
		}
	}
}
JavaString javaString();

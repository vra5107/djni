// D import file generated from 'source/jni/JavaMethod.d'
module jni.JavaMethod;
import std.algorithm;
import std.typetuple;
import jni.jni;
import jni.JniProxy;
import jni.JavaEnv;
import jni.JavaObject;
import jni.JavaClass;
private 
{
	template Fun(size_t i, T)
	{
		alias Fun = TypeTuple!(std.string.format(", ToJavaProxy!(As[%d])(Env(), args[%d]).Val()", i, i));
	}
	template ConcatStrings()
	{
		const Val = "";
	}
	template ConcatStrings(T, As...) if (!is(T == string))
	{
		static assert(T);
	}
	template ConcatStrings(string s, As...)
	{
		const Val = s ~ ConcatStrings!As.Val;
	}
}
struct JavaMethod(T : R function(As), R, As...)
{
	public 
	{
		this(JavaObject obj, jmethodID mid)
		{
			_obj = obj;
			_val = mid;
		}
		this(JavaMethod m)
		{
			swap(_obj, m._obj);
			swap(_val, m._val);
			m._obj = JavaObject();
			m._val = null;
		}
		this(ref JavaMethod m)
		{
			_obj.__ctor(m._obj);
			_val = m._val;
		}
		ref JavaMethod opAssign(JavaMethod m)
		{
			swap(_obj, m._obj);
			swap(_val, m._val);
			return this;
		}
		ref JavaMethod opAssign(ref JavaMethod m)
		{
			_obj = m._obj;
			_val = m._val;
			return this;
		}
		const bool opEquals(ref const(JavaMethod) m)
		{
			return _obj == m._obj && _val == m._val;
		}
		const jmethodID Val()
		{
			return cast(jmethodID)_val;
		}
		const JavaEnv Env()
		{
			return _obj.Env();
		}
		const bool Valid()
		{
			return _obj.Valid() && _val !is null;
		}
		const ref const(JavaObject) GetObject()
		{
			return _obj;
		}
		const JavaClass GetClass()
		{
			return GetObject().GetClass();
		}
		static if (IsPrimitiveJniType!R.Val)
		{
			const R opCall(As args)
			{
				mixin("return Env().Val().Call" ~ JniFuncTypeName!R.Name ~ "Method(GetObject().Val(), Val()" ~ ConcatStrings!(staticIndexedMap!(Fun, As)).Val ~ ");");
			}
		}
		else
		{
			const R opCall(As args)
			{
				mixin("return FromJavaProxy!R(Env(), Env().Val().CallObjectMethod(GetObject().Val(), Val()" ~ ConcatStrings!(staticIndexedMap!(Fun, As)).Val ~ ")).Val();");
			}
		}
		private 
		{
			JavaObject _obj;
			jmethodID _val;
		}
	}
}
struct JavaStaticMethod(T : R function(As), R, As...)
{
	public 
	{
		this(JavaClass cls, jmethodID mid)
		{
			_cls = cls;
			_val = mid;
		}
		this(JavaStaticMethod m)
		{
			swap(_cls, m._cls);
			swap(_val, m._val);
			m._cls = JavaClass();
			m._val = null;
		}
		this(ref JavaStaticMethod m)
		{
			_cls.__ctor(m._cls);
			_val = m._val;
		}
		ref JavaStaticMethod opAssign(JavaStaticMethod m)
		{
			swap(_cls, m._cls);
			swap(_val, m._val);
			return this;
		}
		ref JavaStaticMethod opAssign(ref JavaStaticMethod m)
		{
			_cls = m._cls;
			_val = m._val;
			return this;
		}
		const bool opEquals(ref const(JavaStaticMethod) m)
		{
			return _cls == m._cls && _val == m._val;
		}
		const jmethodID Val()
		{
			return cast(jmethodID)_val;
		}
		const JavaEnv Env()
		{
			return _cls.Env();
		}
		const bool Valid()
		{
			return _cls.Valid() && _val !is null;
		}
		const ref const(JavaClass) GetClass()
		{
			return _cls;
		}
		static if (IsPrimitiveJniType!R.Val)
		{
			const R opCall(As args)
			{
				mixin("return Env().Val().CallStatic" ~ JniFuncTypeName!R.Name ~ "Method(GetClass().Val(), Val()" ~ ConcatStrings!(staticIndexedMap!(Fun, As)).Val ~ ");");
			}
		}
		else
		{
			const R opCall(As args)
			{
				mixin("return FromJavaProxy!R(Env(), Env().Val().CallStaticObjectMethod(GetClass().Val(), Val()" ~ ConcatStrings!(staticIndexedMap!(Fun, As)).Val ~ ")).Val();");
			}
		}
		private 
		{
			JavaClass _cls;
			jmethodID _val;
		}
	}
}

// D import file generated from 'source/jni/JavaField.d'
module jni.JavaField;
import std.algorithm;
import std.typetuple;
import jni.jni;
import jni.JniProxy;
import jni.JavaEnv;
import jni.JavaObject;
import jni.JavaClass;
struct JavaField(T)
{
	public 
	{
		static JavaField opCall()
		{
			return JavaField(JavaObject(), null);
		}
		this(JavaObject obj, jfieldID mid)
		{
			_obj = obj;
			_val = mid;
		}
		this(JavaField m)
		{
			swap(_obj, m._obj);
			swap(_val, m._val);
			m._obj = JavaObject();
			m._val = null;
		}
		this(ref JavaField m)
		{
			_obj.__ctor(m._obj);
			_val = m._val;
		}
		ref JavaField opAssign(JavaField m)
		{
			swap(_obj, m._obj);
			swap(_val, m._val);
			return this;
		}
		ref JavaField opAssign(ref JavaField m)
		{
			_obj = m._obj;
			_val = m._val;
			return this;
		}
		const bool opEquals(ref const(JavaField) m)
		{
			return _obj == m._obj && _val == m._val;
		}
		const jfieldID Val()
		{
			return cast(jfieldID)_val;
		}
		const JavaEnv Env()
		{
			return _obj.Env();
		}
		const bool Valid()
		{
			return _obj.Valid() && _val !is null;
		}
		const ref const(JavaObject) GetObject()
		{
			return _obj;
		}
		const JavaClass GetClass()
		{
			return GetObject().GetClass();
		}
		static if (IsPrimitiveJniType!T.Val)
		{
			const T Get()
			{
				mixin("return Env().Val().Get" ~ JniFuncTypeName!T.Name ~ "Field(GetObject().Val(), Val());");
			}
			void Set(T val)
			{
				mixin("Env().Val().Set" ~ JniFuncTypeName!T.Name ~ "Field(GetObject().Val(), Val(), val);");
			}
		}
		else
		{
			const T Get()
			{
				return FromJavaProxy!T(Env(), Env().Val().GetObjectField(GetObject().Val(), Val())).Val();
			}
			void Set(T val)
			{
				Env().Val().SetObjectField(GetObject().Val(), Val(), ToJavaProxy!T(Env(), val).Val());
			}
		}
		private 
		{
			JavaObject _obj;
			jfieldID _val;
		}
	}
}
struct JavaStaticField(T)
{
	public 
	{
		static JavaStaticField opCall()
		{
			return JavaStaticField(JavaClass(), null);
		}
		this(JavaClass cls, jfieldID mid)
		{
			_cls = cls;
			_val = mid;
		}
		this(JavaStaticField m)
		{
			swap(_cls, m._cls);
			swap(_val, m._val);
			m._cls = JavaClass();
			m._val = null;
		}
		this(ref JavaStaticField m)
		{
			_cls.__ctor(m._cls);
			_val = m._val;
		}
		ref JavaStaticField opAssign(JavaStaticField m)
		{
			swap(_cls, m._cls);
			swap(_val, m._val);
			return this;
		}
		ref JavaStaticField opAssign(ref JavaStaticField m)
		{
			_cls = m._cls;
			_val = m._val;
			return this;
		}
		const bool opEquals(ref const(JavaStaticField) m)
		{
			return _cls == m._cls && _val == m._val;
		}
		const jfieldID Val()
		{
			return cast(jfieldID)_val;
		}
		const JavaEnv Env()
		{
			return _cls.Env();
		}
		const bool Valid()
		{
			return _cls.Valid() && _val !is null;
		}
		const ref const(JavaClass) GetClass()
		{
			return _cls;
		}
		static if (IsPrimitiveJniType!T.Val)
		{
			const T Get()
			{
				mixin("return Env().Val().GetStatic" ~ JniFuncTypeName!T.Name ~ "Field(GetClass().Val(), Val());");
			}
			void Set(T val)
			{
				mixin("Env().Val().SetStatic" ~ JniFuncTypeName!T.Name ~ "Field(GetClass().Val(), Val(), val);");
			}
		}
		else
		{
			const T Get()
			{
				return FromJavaProxy!T(Env(), Env().Val().GetStaticObjectField(GetClass().Val(), Val())).Val();
			}
			void Set(T val)
			{
				Env().Val().SetStaticObjectField(GetClass().Val(), Val(), ToJavaProxy!T(Env(), val).Val());
			}
		}
		private 
		{
			JavaClass _cls;
			jfieldID _val;
		}
	}
}

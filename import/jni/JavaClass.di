// D import file generated from 'source/jni/JavaClass.d'
module jni.JavaClass;
import std.stdio;
import std.algorithm;
import jni.jni;
import jni.JniProxy;
import jni.JavaEnv;
import jni.JavaObject;
import jni.JavaMethod;
import jni.JavaField;
struct JavaClass
{
	public 
	{
		this(JavaEnv env, jclass c)
		{
			writefln("%s(JavaEnv, jclass)", __FUNCTION__);
			base.__ctor(env, c);
		}
		this(JavaEnv env, string val)
		{
			writefln("%s(JavaEnv, string)", __FUNCTION__);
			this(env, GetClass(env, val));
		}
		this(JavaClass cls)
		{
			writefln("%s(JavaClass)", __FUNCTION__);
			swap(base, cls.base);
			cls.base = JavaObject();
		}
		this(ref JavaClass cls)
		{
			writefln("%s(ref JavaClass)", __FUNCTION__);
			base.__ctor(cls.base);
		}
		this(this);
		~this();
		ref JavaClass opAssign(JavaClass v);
		ref JavaClass opAssign(ref JavaClass v);
		const bool opEquals(ref const(JavaClass) cls);
		const bool opEquals(string str);
		const jclass Val();
		const JavaClass Superclass();
		const jboolean IsAssignableFrom(JavaClass cls);
		const jboolean IsAssignableFrom(ref const(JavaClass) cls);
		const JavaMethod!T GetMethod(T)(ref JavaObject obj, string name)
		{
			return JavaMethod!T(obj, Env().Val().GetMethodID(Val(), std.string.toStringz(name), std.string.toStringz(JniSignatureBuilder!T.Sign)));
		}
		JavaStaticMethod!T GetStaticMethod(T)(string name)
		{
			return JavaStaticMethod!T(this, Env().Val().GetStaticMethodID(Val(), std.string.toStringz(name), std.string.toStringz(JniSignatureBuilder!T.Sign)));
		}
		JavaStaticMethod!T GetConstructor(T)()
		{
			return JavaStaticMethod!T(this, Env().Val().GetMethodID(Val(), std.string.toStringz("<init>"), std.string.toStringz(JniSignatureBuilder!T.Sign)));
		}
		JavaStaticMethod!(void function()) GetDefaultConstructor();
		const JavaField!T GetField(T)(ref JavaObject obj, string name)
		{
			return JavaField!T(this, Env().Val().GetFieldID(Val(), std.string.toStringz(name), std.string.toStringz(JniSignatureBuilder!T.Sign)));
		}
		JavaStaticField!T GetStaticField(T)(string name)
		{
			return JavaStaticField!T(this, Env().Val().GetStaticFieldID(Val(), std.string.toStringz(name), std.string.toStringz(JniSignatureBuilder!T.Sign)));
		}
		T GetStaticFieldValue(T)(string name)
		{
			return GetStaticField!T(name).Get();
		}
		JavaObject NewObject(T : R function(As), R, As...)(As args)
		{
			return FromJavaProxy!JavaObject(Env(), Env().Val().NewObject(Val(), GetConstructor!T().Val(), ToJavaProxy(Env(), args).Val())).Val();
		}
		JavaObject base;
		alias base this;
JavaClass javaClass();
		private static jclass GetClass(JavaEnv env, string name);
	}
}

// D import file generated from 'source/jni/JniHelper.d'
module jni.JniHelper;
import jni.jni;
import jni.JavaVM;
struct JniHelper
{
	public 
	{
		static ref JniJavaVM GetJavaVM();
		static ref JniJavaVM GetJavaVM(string classPath);
		static ref JniJavaVM GetJavaVM(string classPath, jint ver);
		static ref JniJavaVM GetJavaVM(JavaVM* vm);
		static ref JniJavaVM GetJavaVM(JavaVM* vm, jint ver);
		private static JniJavaVM* javaVmInstance;
	}
}

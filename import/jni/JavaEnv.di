// D import file generated from 'source/jni/JavaEnv.d'
module jni.JavaEnv;
import std.stdio;
import std.algorithm;
import jni.jni;
import jni.JavaVM;
import jni.JavaClass;
struct JavaEnv
{
	public 
	{
		this(JNIEnv* env)
		{
			val = env;
		}
		JNIEnv* Val();
		const const(JNIEnv)* Val();
		const bool Valid();
		const bool opEquals(JavaEnv env);
		const bool opEquals(ref const(JavaEnv) env);
		const jint Version();
		const JniJavaVM GetVM();
		JavaClass FindClass(string str);
		JavaEnv javaEnv();
		private JNIEnv* val;
	}
}

// D import file generated from 'source/jni/JniProxy.d'
module jni.JniProxy;
import std.typetuple;
import jni.jni;
import jni.JavaEnv;
import jni.JavaObject;
import jni.JavaClass;
import jni.JavaString;
import jni.JavaArray;
template _staticIndexedMap(alias F, size_t N, T...)
{
	static if (T.length == 0)
	{
		alias _staticIndexedMap = TypeTuple!();
	}
	else
	{
		static if (T.length == 1)
		{
			alias _staticIndexedMap = TypeTuple!(F!(N, T[0]));
		}
		else
		{
			alias _staticIndexedMap = TypeTuple!(_staticIndexedMap!(F, N, T[0]), _staticIndexedMap!(F, N + 1, T[1 .. $]));
		}
	}
}
template staticIndexedMap(alias F, T...)
{
	alias staticIndexedMap = _staticIndexedMap!(F, 0, T);
}
template IsPrimitiveJniType(T)
{
	static if (is(T == jboolean) || is(T == jbyte) || is(T == jchar) || is(T == jshort) || is(T == jint) || is(T == jlong) || is(T == jfloat) || is(T == jdouble) || is(T == void))
	{
		const Val = true;
	}
	else
	{
		const Val = false;
	}
}
template JniArrayType(T)
{
	alias type = jarray;
}
template JniArrayType(T : jboolean)
{
	alias type = jbooleanArray;
}
template JniArrayType(T : jbyte)
{
	alias type = jbyteArray;
}
template JniArrayType(T : jchar)
{
	alias type = jcharArray;
}
template JniArrayType(T : jshort)
{
	alias type = jshortArray;
}
template JniArrayType(T : jint)
{
	alias type = jintArray;
}
template JniArrayType(T : jlong)
{
	alias type = jlongArray;
}
template JniArrayType(T : jfloat)
{
	alias type = jfloatArray;
}
template JniArrayType(T : jdouble)
{
	alias type = jdoubleArray;
}
template JniArrayType(T : jobject)
{
	alias type = jobjectArray;
}
template JniFuncTypeName(T)
{
	static assert(false);
}
template JniFuncTypeName(T : void)
{
	const Name = "Void";
}
template JniFuncTypeName(T : jboolean)
{
	const Name = "Boolean";
}
template JniFuncTypeName(T : jbyte)
{
	const Name = "Byte";
}
template JniFuncTypeName(T : jchar)
{
	const Name = "Char";
}
template JniFuncTypeName(T : jshort)
{
	const Name = "Short";
}
template JniFuncTypeName(T : jint)
{
	const Name = "Int";
}
template JniFuncTypeName(T : jlong)
{
	const Name = "Long";
}
template JniFuncTypeName(T : jfloat)
{
	const Name = "Float";
}
template JniFuncTypeName(T : jdouble)
{
	const Name = "Double";
}
template JniFuncTypeName(T : jobject)
{
	const Name = "Object";
}
template JniSignatureBuilder(T)
{
	static assert(false);
}
template JniSignatureBuilder()
{
	const Sign = "";
}
template JniSignatureBuilder(T, Ts...)
{
	const Sign = JniSignatureBuilder!T.Sign ~ JniSignatureBuilder!Ts.Sign;
}
template JniSignatureBuilder(T : void)
{
	const Sign = "V";
}
template JniSignatureBuilder(T : jboolean)
{
	const Sign = "Z";
}
template JniSignatureBuilder(T : jbyte)
{
	const Sign = "B";
}
template JniSignatureBuilder(T : jchar)
{
	const Sign = "C";
}
template JniSignatureBuilder(T : jshort)
{
	const Sign = "S";
}
template JniSignatureBuilder(T : jint)
{
	const Sign = "I";
}
template JniSignatureBuilder(T : jlong)
{
	const Sign = "J";
}
template JniSignatureBuilder(T : jfloat)
{
	const Sign = "F";
}
template JniSignatureBuilder(T : jdouble)
{
	const Sign = "D";
}
template JniSignatureBuilder(T : JavaObject)
{
	const Sign = "Ljava/lang/Object;";
}
template JniSignatureBuilder(T : JavaBoolean)
{
	const Sign = "Ljava/lang/Boolean;";
}
template JniSignatureBuilder(T : JavaString)
{
	const Sign = "Ljava/lang/String;";
}
template JniSignatureBuilder(T : JavaArray!U, U)
{
	const Sign = "[" ~ JniSignatureBuilder!U.Sign;
}
template JniSignatureBuilder(T : string)
{
	const Sign = JniSignatureBuilder!JavaString.Sign;
}
template JniSignatureBuilder(T : U[], U)
{
	const Sign = JniSignatureBuilder!(JavaArray!U, U).Sign;
}
template JniSignatureBuilder(T : R function(As), R, As...)
{
	const Sign = "(" ~ JniSignatureBuilder!As.Sign ~ ")" ~ JniSignatureBuilder!R.Sign;
}
struct JniToJavaProxy(T)
{
	this(JavaEnv, T v)
	{
		val = v;
	}
	const T Val()
	{
		return val;
	}
	T val;
}
struct JniToJavaProxy(T : JavaObject)
{
	this(JavaEnv, ref T v)
	{
		val = v;
	}
	const jobject Val()
	{
		return val.Val();
	}
	T val;
}
struct JniToJavaProxy(T : JavaBoolean)
{
	this(JavaEnv, ref T v)
	{
		val = v;
	}
	const jobject Val()
	{
		return val.Val();
	}
	T val;
}
struct JniToJavaProxy(T : JavaClass)
{
	this(JavaEnv, ref T v)
	{
		val = v;
	}
	const jobject Val()
	{
		return val.Val();
	}
	T val;
}
struct JniToJavaProxy(T : JavaString)
{
	this(JavaEnv, ref T v)
	{
		val = v;
	}
	const jobject Val()
	{
		return val.Val();
	}
	T val;
}
struct JniToJavaProxy(T : JavaArray!U, U)
{
	this(JavaEnv, ref T v)
	{
		val = v;
	}
	const jobject Val()
	{
		return val.Val();
	}
	T val;
}
struct JniToJavaProxy(T : string)
{
	this(JavaEnv env, ref T v)
	{
		val.__ctor(env, v);
	}
	const jobject Val()
	{
		return val.Val();
	}
	JavaString val;
}
struct JniToJavaProxy(T : U[], U)
{
	this(JavaEnv env, ref T v)
	{
		val.__ctor(env, v);
	}
	const jobject Val()
	{
		return val.Val();
	}
	JavaArray!U val;
}
import std.typecons : tuple;
auto ToJavaProxy(T)(JavaEnv env, T v)
{
	return JniToJavaProxy!T(env, v);
}
auto ToJavaProxy(Ts...)(JavaEnv env, Ts ts) if (ts.length > 1)
{
	return tuple(ToJavaProxy!(Ts[0])(env, ts[0]).tupleof, ToJavaProxy!(Ts[1 .. $])(env, ts[1..$]).tupleof);
}
struct JniFromJavaProxy(T : JavaObject)
{
	this(JavaEnv env, jobject obj)
	{
		_env = env;
		_obj = obj;
	}
	T Val()
	{
		return T(_env, _obj);
	}
	JavaEnv _env;
	jobject _obj;
}
struct JniFromJavaProxy(T : JavaBoolean)
{
	this(JavaEnv env, jobject obj)
	{
		_env = env;
		_obj = obj;
	}
	T Val()
	{
		return T(_env, _obj);
	}
	JavaEnv _env;
	jobject _obj;
}
struct JniFromJavaProxy(T : JavaClass)
{
	this(JavaEnv env, jobject obj)
	{
		_env = env;
		_obj = cast(jclass)obj;
	}
	T Val()
	{
		return T(_env, _obj);
	}
	JavaEnv _env;
	jclass _obj;
}
struct JniFromJavaProxy(T : JavaString)
{
	this(JavaEnv env, jobject obj)
	{
		_env = env;
		_obj = cast(jstring)obj;
	}
	T Val()
	{
		return T(_env, _obj);
	}
	JavaEnv _env;
	jstring _obj;
}
struct JniFromJavaProxy(T : JavaArray!U, U)
{
	this(JavaEnv env, jobject obj)
	{
		_env = env;
		_obj = cast(JniArrayType!U.type)obj;
	}
	T Val()
	{
		return T(_env, _obj);
	}
	JavaEnv _env;
	JniArrayType!U.type _obj;
}
struct JniFromJavaProxy(T : string)
{
	this(JavaEnv env, jobject obj)
	{
		_env = env;
		_obj = cast(jstring)obj;
	}
	T Val()
	{
		return JavaString(_env, _obj).Value();
	}
	JavaEnv _env;
	jstring _obj;
}
struct JniFromJavaProxy(T : U[], U)
{
	this(JavaEnv env, jobject obj)
	{
		_env = env;
		_obj = cast(JniArrayType!U.type)obj;
	}
	T Val()
	{
		return JniFromJavaProxy!(JavaArray!U, U)(_env, _obj).Val().ToVector();
	}
	JavaEnv _env;
	JniArrayType!U.type _obj;
}
auto FromJavaProxy(T, J)(JavaEnv env, J v)
{
	return JniFromJavaProxy!T(env, v);
}

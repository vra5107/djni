// D import file generated from 'source/jni/JavaObject.d'
module jni.JavaObject;
import std.stdio;
import std.algorithm;
import jni.jni;
import jni.JniProxy;
import jni.JavaEnv;
import jni.JavaClass;
import jni.JavaMethod;
import jni.JavaField;
version = MSYM_DEBUG_JNI_REF_CNT;
version (MSYM_DEBUG_JNI_REF_CNT)
{
	int globalRefCnt = 0;
	int maxGlobalRefCnt = 0;
}
struct JavaObject
{
	public 
	{
		this(JavaEnv env, jobject o)
		{
			writefln("%s(JavaEnv, jobject)", __FUNCTION__);
			_env = env;
			_obj = o;
			version (MSYM_DEBUG_JNI_REF_CNT)
			{
				if (Valid())
				{
					maxGlobalRefCnt++;
					globalRefCnt++;
				}
			}

		}
		this(JavaObject o)
		{
			writefln("%s(JavaObject)", __FUNCTION__);
			_env = o._env;
			_obj = o._obj;
			o._env = JavaEnv();
			o._obj = null;
		}
		this(ref JavaObject o)
		{
			writefln("%s(ref JavaObject)", __FUNCTION__);
			Init(o);
		}
		this(this);
		~this();
		ref JavaObject opAssign(JavaObject v);
		ref JavaObject opAssign(ref JavaObject v);
		const bool opEquals(ref const(JavaObject) v);
		const JavaClass GetClass();
		const bool Valid();
		const JavaEnv Env();
		const jobject Val();
		const jint MonitorEnter();
		const jint MonitorExit();
		JavaMethod!T GetMethod(T)(string name)
		{
			return GetClass().GetMethod!T(this, name);
		}
		JavaField!T GetField(T)(string name)
		{
			return GetClass().GetField!T(this, name);
		}
		T GetFieldValue(T)(string name)
		{
			return GetField!T(name).Get();
		}
		package 
		{
			void Reset(ref const(JavaObject) o);
			private 
			{
				void Init(ref const(JavaObject) o);
				void Destroy();
				package 
				{
					JavaEnv _env;
					jobject _obj;
				}
			}
		}
	}
}
JavaObject javaObject();
struct JavaBoolean
{
	public 
	{
		this(JavaEnv env, jobject c)
		{
			writefln("%s(JavaEnv, jobject)", __FUNCTION__);
			base.__ctor(env, c);
		}
		this(JavaBoolean cls)
		{
			writefln("%s(JavaBoolean)", __FUNCTION__);
			swap(base, cls.base);
			cls.base = JavaObject();
		}
		this(ref JavaBoolean cls)
		{
			writefln("%s(ref JavaBoolean)", __FUNCTION__);
			base.__ctor(cls.base);
		}
		this(this);
		~this();
		ref JavaBoolean opAssign(JavaBoolean v);
		ref JavaBoolean opAssign(ref JavaBoolean v);
		JavaObject base;
		alias base this;
}
}
JavaBoolean javaBoolean();
